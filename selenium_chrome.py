#!/usr/bin/python3
from selenium import webdriver
import getpass
import time


try:
    chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument('--headless')
    dr = webdriver.Chrome(chrome_options=chrome_options)
    dr.maximize_window()

    # dr.get(url)
    # dr.find_element_by_xpath()
    # dr.find_element_by_id()
    # dr.find_element_by_name()
    # dr.find_element_by_xpath()
    # dr.find_element_by_link_text()
    # dr.find_element_by_partial_link_text()
    # dr.find_element_by_tag_name()
    # dr.find_element_by_class_name()
    # dr.find_element_by_css_selector()

    # dr.find_elements_by_name()
    # dr.find_elements_by_xpath()
    # dr.find_elements_by_link_text()
    # dr.find_elements_by_partial_link_text()
    # dr.find_elements_by_tag_name()
    # dr.find_elements_by_class_name()
    # dr.find_elements_by_css_selector()

    # dr.execute_script()

    # element.send_keys(value)
    # element.click()

    # Obtenemos user y password
    user = input('User:')
    password = getpass.getpass()

except Exception as e:
    print(e)
finally:
    dr.quit()
